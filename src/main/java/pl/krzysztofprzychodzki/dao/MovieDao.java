package pl.krzysztofprzychodzki.dao;

import pl.krzysztofprzychodzki.model.Movie;

import java.util.List;
import java.util.UUID;

/**
 *
 */

public interface MovieDao {

    void save(Movie movie);

    void update(Movie movie);

    void delete(UUID uuid);

    List<Movie> findAll();

    List<Movie> findAllWatched(boolean isWatched);

    Movie findByTitle(String title);
}
