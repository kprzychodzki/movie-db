package pl.krzysztofprzychodzki.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.krzysztofprzychodzki.model.User;
import pl.krzysztofprzychodzki.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 */
@Transactional
@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private UserDaoCrud userDaoCrud;

    @Autowired
    private UserRepository userRepository;

    @Override
    public void save(User user) {
        userDaoCrud.save(user);
    }

    @Override
    public List<User> findAll() {
        Iterable<User> usersCollection = userDaoCrud.findAll();
        List<User> usersList = new ArrayList<>();
        usersCollection.forEach(usersList::add);
        return usersList;

    }

    @Override
    public User findByUsername(String username) {
        return findAll().stream().filter(user -> username.equals(user.getName())).findFirst().get();
    }


    @Override
    public void delete(UUID uuid) {
        userDaoCrud.deleteById(uuid);
    }


}
