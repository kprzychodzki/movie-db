package pl.krzysztofprzychodzki.dao;


import org.springframework.data.repository.CrudRepository;
import pl.krzysztofprzychodzki.model.Movie;

import java.util.UUID;

/**
 *
 */

public interface MovieDaoCrud extends CrudRepository<Movie, UUID> {

}
