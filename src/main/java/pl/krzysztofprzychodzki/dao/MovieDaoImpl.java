package pl.krzysztofprzychodzki.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.krzysztofprzychodzki.model.Movie;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 *
 */

@Transactional
@Repository
public class MovieDaoImpl implements MovieDao {

    @Autowired
    private MovieDaoCrud movieDaoCrud;

    @Override
    public void save(Movie movie) {
        movieDaoCrud.save(movie);
    }

    @Override
    public void update(Movie movie) {
        this.save(movie);
    }

    @Override
    public void delete(UUID uuid) {
        movieDaoCrud.deleteById(uuid);
    }

    @Override
    public List<Movie> findAll() {
        Iterable<Movie> moviesCollection = movieDaoCrud.findAll();
        List<Movie> moviesList = new LinkedList<>();
        moviesCollection.forEach(moviesList::add);
        return moviesList;
    }

    @Override
    public List<Movie> findAllWatched(boolean isWatched) {
        return findAll().stream().filter(m -> m.isWatched() == isWatched).collect(Collectors.toList());
    }

    @Override
    public Movie findByTitle(String title) {
        return findAll().stream().filter(movie -> title.equals(movie.getTitle())).findAny().get();
    }


}

