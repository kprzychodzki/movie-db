package pl.krzysztofprzychodzki.dao;

import org.springframework.data.repository.CrudRepository;
import pl.krzysztofprzychodzki.model.User;

import java.util.UUID;

/**
 *
 */

public interface UserDaoCrud extends CrudRepository<User, UUID> {

}
