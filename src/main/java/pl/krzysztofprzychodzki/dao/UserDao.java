package pl.krzysztofprzychodzki.dao;

import pl.krzysztofprzychodzki.model.User;

import java.util.List;
import java.util.UUID;

/**
 *
 */

public interface UserDao {

    void save(User user);

    void delete(UUID uuid);

    List<User> findAll();

    User findByUsername(String username);


}
