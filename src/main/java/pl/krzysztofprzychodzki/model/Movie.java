package pl.krzysztofprzychodzki.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

/**
 *
 */

@Entity
public class Movie {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "Binary(16)")
    private UUID id;

    private String title;
    private String description;
    private boolean watched;

    @JsonIgnore
    @ManyToOne
    private User user;


    public Movie() {
    }

    public Movie(String title, String description, boolean watched, User user) {
        this.title = title;
        this.description = description;
        this.watched = watched;
        this.user = user;
    }

    public UUID getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public boolean isWatched() {
        return watched;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
