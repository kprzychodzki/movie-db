package pl.krzysztofprzychodzki.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.krzysztofprzychodzki.model.User;

import java.util.UUID;

/**
 *
 */

public interface UserRepository extends JpaRepository<User, UUID> {
    User findOneByUsername(String username);
}
