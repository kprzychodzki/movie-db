package pl.krzysztofprzychodzki.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.krzysztofprzychodzki.dao.MovieDao;
import pl.krzysztofprzychodzki.dao.UserDao;
import pl.krzysztofprzychodzki.model.Movie;
import pl.krzysztofprzychodzki.model.User;


import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 *
 */

@RestController
@RequestMapping("/api/v1")
public class MovieController {


    @Autowired
    private MovieDao movieDao;

    @Autowired
    private UserDao userDao;


    @RequestMapping(value = "/movies", method = RequestMethod.GET, produces = "application/json")
    public List<Movie> listAllMovies() {
        return movieDao.findAll();
    }

    @RequestMapping(value = "/movies", method = RequestMethod.POST)
    public ResponseEntity<Movie> createMovie(@RequestBody Movie movie, Principal principal) {
        User currentUser = userDao.findByUsername(principal.getName());
        Movie currentMovie = new Movie(movie.getTitle(),movie.getDescription(),movie.isWatched(), currentUser);
        Set<Movie> userMovie = new HashSet<>();
        userMovie.add(currentMovie);
        currentUser.setMovieSet(userMovie);
        movieDao.save(currentMovie);
        return new ResponseEntity<Movie>(currentMovie, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/movies/watched/{isWatched}", method = RequestMethod.GET)
    public List<Movie> allWatched(@PathVariable boolean isWatched) {
        return movieDao.findAllWatched(isWatched);
    }

    @RequestMapping(value = "movies/{title}", method = RequestMethod.DELETE)
    public ResponseEntity<Movie> deleteMovie(@PathVariable String title) {
        Movie movie = movieDao.findByTitle(title);
        movieDao.delete(movie.getId());
        return new ResponseEntity<Movie>(HttpStatus.OK);
    }

    @RequestMapping(value = "movies/{title}", method = RequestMethod.PUT)
    public ResponseEntity<Movie> updateMovie(@PathVariable String title) {
        movieDao.update(movieDao.findByTitle(title));
        return new ResponseEntity<Movie>(HttpStatus.OK);
    }


}
