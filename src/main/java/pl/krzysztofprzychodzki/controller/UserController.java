package pl.krzysztofprzychodzki.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import pl.krzysztofprzychodzki.dao.UserDao;
import pl.krzysztofprzychodzki.model.User;
import pl.krzysztofprzychodzki.repository.UserRepository;
import pl.krzysztofprzychodzki.service.UserService;

import java.security.Principal;
import java.util.List;
import java.util.UUID;

/**
 *
 */
@RestController
@RequestMapping("/api/v1")
public class UserController {

    @Autowired
    private UserDao userDao;


    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<User> listAllUsers() {
        return userDao.findAll();
    }

    @RequestMapping(value = "/users/{username}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteCurrentUser(@PathVariable String username, Principal principal) {

        User currentUser = userDao.findByUsername(principal.getName());
        if (username.equals(currentUser.getUsername())) {
            userDao.delete(currentUser.getId());
            return new ResponseEntity<Void>(HttpStatus.OK);
        } else {
            return new ResponseEntity<Void>(HttpStatus.UNAUTHORIZED);
        }
    }

    @RequestMapping(value = "/users/active", method = RequestMethod.GET)
    public ResponseEntity<?> activeUser(Principal principal) {
        if (principal == null) {
            return new ResponseEntity<>("ggggg", HttpStatus.IM_USED);
        } else {
            User currentUser = userDao.findByUsername(principal.getName());

            return new ResponseEntity<>(currentUser, HttpStatus.OK);
        }
    }

//    @RequestMapping(value = "/users/{username}", method = RequestMethod.DELETE)
//    public ResponseEntity<?> deleteUserByUsername(@PathVariable String username) {
//        User user = userDao.findByUsername(username);
//        userDao.delete(user.getId());
//        return new ResponseEntity<>(HttpStatus.OK);
//    }

//    @RequestMapping(value = "/users/{uuid}", method = RequestMethod.DELETE)
//    public ResponseEntity<User> deleteUser(@PathVariable UUID uuid) {
//        userDao.delete(uuid);
//        return new ResponseEntity<User>(HttpStatus.OK);
//    }


}
