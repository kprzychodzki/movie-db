INSERT INTO moviedb.user (id, username, password, enabled) VALUES
  (unhex(replace(uuid(), '-', '')), 'tytus', '$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri', TRUE),
  (unhex(replace(uuid(), '-', '')), 'romek', '$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri', TRUE),
  (unhex(replace(uuid(), '-', '')), 'atomek', '$2a$10$D4OLKI6yy68crm.3imC9X.P2xqKHs5TloWUcr6z5XdOqnTrAK84ri', TRUE);

/*# password = password*/

INSERT INTO moviedb.movie (id, description, title, watched) VALUES
  (unhex(replace(uuid(), '-', '')), 'First part of best time travel trilogy', 'Back to the future Part 1', TRUE),
  (unhex(replace(uuid(), '-', '')), 'Great movie with Steven Segal', 'Nico', TRUE),
  (unhex(replace(uuid(), '-', '')), 'Tron is a 1982 American science fiction action-adventure film.', 'Tron', FALSE);