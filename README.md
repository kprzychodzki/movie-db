# Movie DB #

JSON API service to store and manage simple movie database.

### Functional requirements ###

* Users are able to register (default 3 users: tytus, romek and atomek) with OAuth2.
* Users are able to remove their account (DELETE @ http://localhost:8080/api/v1/users/{username})
* Users are able to add new movie (POST @ http://localhost:8080/api/v1/movies)
* Users should be able to list all entries.
* Users should be able to filter movies by w atched flag (all, watched, unwatched).
* Users are not be able to edit movies - this one needs to be implemented.
* Users should be able to delete movies.

### How to run this project ###

```
mvn spring-boot:run
```

### How to get token ###

```
curl -X POST --user 'moviedb:secret' -d 'grant_type=password&username=tytus&password=password' http://localhost:8080/oauth/token
```
or using *Postman* application:

| Key | Value |
| ----- | ----- |
| **method:** | POST | 
| **url:** | http://localhost:8080/oauth/token |
| **headers:** | Content-type = application/x-www-form-urlencoded, Authorization = Basic <Base64 encoded string in the form of 'client-id:client-secret'> |
| **body** | (form url encoded): grant_type = password, username = <username of the user>, password = <user password> |

 
### Examples ###
 
Get token:
 
```
curl -X POST --user 'moviedb:secret' -d 'grant_type=password&username=tytus&password=password' http://localhost:8080/oauth/token
```

result:
```
    {
         "access_token": "9ffb6210-5582-48d1-802c-fb6739109124",
         "token_type": "bearer",
         "refresh_token": "88a0a9e0-d768-4069-9b9f-179c68089a27",
         "expires_in": 3417,
         "scope": "read write"
    }
```
 
Add new movie:
  
```
  curl -i -H "Accept: application/json" \
  		-H "Content-Type: application/json" \
  		-H "Authorization: Bearer 9ffb6210-5582-48d1-802c-fb6739109124" \
  		-X POST -d {"title": "Futurama - Bender's Big Score",
          "description": "Planet Express sees a hostile takeover and Bender falls into the hands of criminals where he is used to fulfill their schemes.",
          "watched": true} http://localhost:8080/api/v1/movies
```
  
Get movies list:
 
```
curl -i -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer 9ffb6210-5582-48d1-802c-fb6739109124" -X GET http://localhost:8080/api/v1/movies
```
result:
 
```
    {
              "id": "ce346ef4-6d84-11e7-b7f2-7993748c603e",
              "title": "Back to the future Part 1",
              "description": "First part of best time travel trilogy",
              "watched": true
          },
          {
              "id": "ce347070-6d84-11e7-b7f2-7993748c603e",
              "title": "Nico",
              "description": "Great movie with Steven Segal",
              "watched": true
          },
          {
              "id": "ce3470e8-6d84-11e7-b7f2-7993748c603e",
              "title": "Tron",
              "description": "Tron is a 1982 American science fiction action-adventure film.",
              "watched": false
          },
          {
              "id": "22e132eb-73af-4d35-9ce2-f5a1bf35dd1b",
              "title": "Futurama - Bender's Big Score",
              "description": "Planet Express sees a hostile takeover and Bender falls into the hands of criminals where he is used to fulfill their schemes.",
              "watched": true
          }
    }
```



